package com.mama.service;

import com.mama.domain.Region;
import com.mama.dto.RegionForm;
import com.mama.repository.RegionRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class RegionServiceImpl implements RegionService {

  private final RegionRepository regionRepository;

  @Override
  @Transactional
  public Region getOrCreate(String name) {

    Region region = regionRepository.getByName(name);

    if (region == null) {
      Region newRegion = Region.builder()
          .name(name)
          .build();
      region = regionRepository.save(newRegion);
    }

    return region;
  }

  @Override
  public Region save(Region region) {

    if (region == null) {
      throw new IllegalArgumentException("Region is null");
    }

    return regionRepository.save(region);
  }

  @Override
  public Region getById(Long id) {

    return regionRepository.findById(id)
        .orElseThrow(() -> new IllegalArgumentException("Region not found"));
  }

  @Override
  public void update(Long id, RegionForm regionForm) {

    Region region = getById(id);
    region.setName(regionForm.getName());
    save(region);
  }

  @Override
  public List<Region> getAll() {

    return regionRepository.findAll();
  }
}
