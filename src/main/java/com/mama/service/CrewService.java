package com.mama.service;

import com.mama.domain.Crew;
import com.mama.dto.CrewForm;
import java.util.List;

public interface CrewService {

  Crew getById(Long id);

  void delete(Long id);

  Crew save(Crew crew);

  void update(Long id, CrewForm crewForm);

  List<Crew> getAll();
}
