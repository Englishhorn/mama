package com.mama.service;

import com.mama.domain.Skill;
import com.mama.dto.SkillForm;
import java.util.List;

public interface SkillService {

  Skill getOrCreate(String name);

  Skill save(Skill skill);

  Skill getById(Long id);

  void update(Long id, SkillForm skillForm);

  List<Skill> getAll();
}
