package com.mama.controller;

import com.mama.domain.Crew;
import com.mama.service.CrewService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@RequiredArgsConstructor
public class CrewController {

  private final CrewService crewService;

  @GetMapping("/")
  public String getCrews(Model model) {

    model.addAttribute("crews", crewService.getAll());

    return "crews.html";
  }

  @GetMapping("/crews/{id}")
  public String getCrew(@PathVariable("id") Long id, Model model) {

    Crew crew = crewService.getById(id);

    model.addAttribute("crew", crew);

    return "crew.html";
  }
}
