package com.mama.controller.rest;

import com.mama.converter.SkillConverter;
import com.mama.domain.Skill;
import com.mama.dto.SkillForm;
import com.mama.dto.SkillView;
import com.mama.service.SkillService;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/skill")
@RequiredArgsConstructor
public class SkillRestController {

  private final SkillService skillService;
  private final SkillConverter skillConverter;

  @PostMapping
  public Long saveSkill(@RequestBody @Valid SkillForm skillForm) {

    Skill skill = skillService.getOrCreate(skillForm.getName());
    Skill newSkill = skillService.save(skill);

    return newSkill.getId();
  }

  @PutMapping("/{id}")
  public void updateSkill(@PathVariable("id") Long id, @RequestBody @Valid SkillForm skillForm) {

    skillService.update(id, skillForm);
  }

  @GetMapping("/all")
  public List<SkillView> getSkills() {

    return skillService.getAll().stream()
        .map(skillConverter::convertToView)
        .collect(Collectors.toList());
  }
}
