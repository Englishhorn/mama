package com.mama.controller.rest;

import com.mama.converter.RegionConverter;
import com.mama.domain.Region;
import com.mama.dto.RegionForm;
import com.mama.dto.RegionView;
import com.mama.service.RegionService;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/region")
@RequiredArgsConstructor
public class RegionRestController {

  private final RegionService regionService;
  private final RegionConverter regionConverter;

  @PostMapping
  public Long saveRegion(@RequestBody @Valid RegionForm regionForm) {

    Region region = regionService.getOrCreate(regionForm.getName());
    Region newRegion = regionService.save(region);

    return newRegion.getId();
  }

  @PutMapping("/{id}")
  public void updateRegion(@PathVariable("id") Long id, @RequestBody @Valid RegionForm regionForm) {

    regionService.update(id, regionForm);
  }

  @GetMapping("/all")
  public List<RegionView> getRegions() {

    return regionService.getAll().stream()
        .map(regionConverter::convertToView)
        .collect(Collectors.toList());
  }
}
