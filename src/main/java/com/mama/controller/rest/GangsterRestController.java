package com.mama.controller.rest;

import com.mama.converter.GangsterConverter;
import com.mama.domain.Gangster;
import com.mama.dto.GangsterForm;
import com.mama.dto.GangsterView;
import com.mama.service.GangsterService;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/gangster")
@RequiredArgsConstructor
public class GangsterRestController {

  private final GangsterService gangsterService;
  private final GangsterConverter gangsterConverter;

  @GetMapping("/{id}")
  public GangsterView getGangster(@PathVariable("id") Long id) {

    Gangster gangster = gangsterService.getById(id);

    return gangsterConverter.convertToView(gangster);
  }

  @GetMapping("/all")
  public List<GangsterView> getGangsters() {

    return gangsterService.getAll().stream()
        .map(gangsterConverter::convertToView)
        .collect(Collectors.toList());
  }

  @PostMapping
  public Long saveGangster(@RequestBody @Valid GangsterForm gangsterForm) {

    Gangster gangster = gangsterConverter.convertToEntity(gangsterForm);
    Gangster newGangster = gangsterService.save(gangster);

    return newGangster.getId();
  }

  @PutMapping("/{id}")
  public void updateGangster(@PathVariable("id") Long id, @RequestBody @Valid GangsterForm gangsterForm) {

    gangsterService.update(id, gangsterForm);
  }

  @DeleteMapping("/{id}")
  public void deleteGangster(@PathVariable("id") Long id) {

    gangsterService.delete(id);
  }
}
