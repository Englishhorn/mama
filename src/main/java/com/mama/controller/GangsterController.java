package com.mama.controller;

import com.mama.service.GangsterService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class GangsterController {

  private final GangsterService gangsterService;

  @GetMapping("/gangsters")
  public String getGangsters(Model model) {

    model.addAttribute("gangsters", gangsterService.getAll());

    return "gangsters.html";
  }
}
