package com.mama.dto;

import com.mama.domain.Role;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GangsterForm {

  @NotNull
  private String fullName;

  @NotNull
  private Role role;
  private String comment;

  @NotNull
  private List<Long> skillsId;
}
