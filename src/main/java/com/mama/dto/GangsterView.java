package com.mama.dto;

import com.mama.domain.Role;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GangsterView {

  private Long id;
  private String fullName;
  private Role role;
  private String comment;
  private List<SkillView> skills;
}
