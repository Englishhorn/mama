package com.mama.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CrewView {

  private Long id;
  private String name;
  private RegionView region;
  private String occupation;
  private String comment;
  private List<GangsterView> gangsters;
}
