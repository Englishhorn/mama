package com.mama.converter;

import com.mama.domain.Region;
import com.mama.dto.RegionForm;
import com.mama.dto.RegionView;
import org.springframework.stereotype.Component;

@Component
public class RegionConverter {

  public Region convertToEntity(RegionForm regionForm) {

    return Region.builder()
        .name(regionForm.getName())
        .build();
  }

  public RegionView convertToView(Region region) {

    return RegionView.builder()
        .id(region.getId())
        .name(region.getName())
        .build();
  }
}
