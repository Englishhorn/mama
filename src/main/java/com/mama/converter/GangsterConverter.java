package com.mama.converter;

import com.mama.domain.Gangster;
import com.mama.domain.Skill;
import com.mama.dto.GangsterForm;
import com.mama.dto.GangsterView;
import com.mama.dto.SkillView;
import com.mama.service.SkillService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GangsterConverter {

  private final SkillConverter skillConverter;
  private final SkillService skillService;

  public Gangster convertToEntity(GangsterForm gangsterForm) {

    List<Skill> skills = gangsterForm.getSkillsId().stream()
        .map(skillService::getById)
        .collect(Collectors.toList());

    String comment = gangsterForm.getComment() == null ? "" : gangsterForm.getComment();

    return Gangster.builder()
        .fullName(gangsterForm.getFullName())
        .role(gangsterForm.getRole())
        .comment(comment)
        .skills(skills)
        .build();
  }

  public GangsterView convertToView(Gangster gangster) {

    List<SkillView> skills = gangster.getSkills()
        .stream()
        .map(skillConverter::convertToView)
        .collect(Collectors.toList());

    return GangsterView.builder()
        .id(gangster.getId())
        .fullName(gangster.getFullName())
        .role(gangster.getRole())
        .comment(gangster.getComment())
        .skills(skills)
        .build();
  }
}
