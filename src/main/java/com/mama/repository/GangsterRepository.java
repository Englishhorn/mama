package com.mama.repository;

import com.mama.domain.Gangster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GangsterRepository extends JpaRepository<Gangster, Long> {

}
