package com.mama.domain;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Crew {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column
  private String name;

  @ManyToOne
  @JoinColumn(name = "region_id", foreignKey = @ForeignKey(name = "region_id_fk"))
  private Region region;

  @Column
  private String occupation;

  @Column
  private String comment;

  @OneToMany(mappedBy = "crew", cascade = CascadeType.MERGE)
  private List<Gangster> gangsters;
}
