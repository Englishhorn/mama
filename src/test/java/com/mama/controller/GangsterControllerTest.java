package com.mama.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;

import com.mama.domain.Gangster;
import com.mama.service.GangsterService;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;

@ExtendWith(MockitoExtension.class)
class GangsterControllerTest {

  @Captor
  ArgumentCaptor<List<Gangster>> argumentCaptor;

  @Mock
  Model model;

  @Mock
  GangsterService gangsterService;

  @InjectMocks
  GangsterController gangsterController;

  @Test
  void getGangsters_htmlExists_stringReturned() {

    // GIVEN
    Gangster gangster = new Gangster();
    Mockito.when(gangsterService.getAll()).thenReturn(List.of(gangster));

    // WHEN
    String result = gangsterController.getGangsters(model);

    // THEN
    assertEquals("gangsters.html", result);

    Mockito.verify(model).addAttribute(eq("gangsters"), argumentCaptor.capture());
    List<Gangster> gangsters = argumentCaptor.getValue();

    assertEquals(1, gangsters.size());
    assertEquals(gangster, gangsters.get(0));
  }
}
