# MAMA (Mafia Management)
Planned duration: ~ 3 weeks

## Functional requirements:
**The application should have following functionality:**
* Creation and modification of crews and members by supervisors
* Assigning members to crews
* Web pages with info about each project and each member

Technologies: Java 11, Spring Boot, Hibernate, Maven, Junit 5, Bootstrap 4

## General project requirements:
* Follow all naming conventions and keep project well-structured
* Use Layered and MVC architecture patterns
* Try to use GoF patterns where applicable
* Develop Frontend UI
* Write unit tests
* Use Git as VCS
* Follow Agile principles
* Data should be stored in DB

## For developers:
* For database connection necessary copy file info :src/main/resources/application.yml.example to application.yml and fill fields username and password
